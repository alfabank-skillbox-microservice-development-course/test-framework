package main.steps;

import io.qameta.allure.Step;
import io.restassured.RestAssured;
import main.endpoints.UserserviceEndpoints;
import main.entities.Follow;
import main.entities.User;
import main.helpers.JsonHelper;
import main.specifications.UserserviceSpecifications;
import org.instancio.Instancio;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static org.instancio.Select.field;

public class UserserviceSteps {

    @Step("Создание подписки пользователя c id {uuid1} на пользователя c id {uuid2}")
    public static Follow createFollow(UUID uuid1, UUID uuid2) {
        String result = RestAssured.given()
                .with().spec(new UserserviceSpecifications().requestSpec)
                .pathParam("id", uuid1)
                .queryParam("followingUserId", uuid2.toString())
                .post(UserserviceEndpoints.USERS + "/{id}/follows")
                .then()
                .spec(new UserserviceSpecifications().responseSpec)
                .extract()
                .response().getBody().asString();
        return JsonHelper.fromJson(Follow.class, result);
    }

    @Step("Поиск всех подписок пользователя с id {uuid}")
    public static List<Follow> findFollows(UUID uuid) throws IOException, ClassNotFoundException {
        String result = RestAssured.given()
                .with().spec(new UserserviceSpecifications().requestSpec)
                .pathParam("id", uuid)
                .get(UserserviceEndpoints.USERS + "/{id}/follows")
                .then()
                .spec(new UserserviceSpecifications().responseSpec)
                .extract()
                .response().getBody().asString();
        return JsonHelper.fromJsonArray(result, Follow.class);

    }

    @Step("Удаление подписки пользователя c id {uuid1} на пользователя c id {uuid2}")
    public static String deleteFollow(UUID uuid1, UUID uuid2) {
        return RestAssured.given()
                .with().spec(new UserserviceSpecifications().requestSpec)
                .pathParam("id", uuid1)
                .queryParam("followingUserId", uuid2)
                .delete(UserserviceEndpoints.USERS + "/{id}/follows")
                .then()
                .spec(new UserserviceSpecifications().responseSpec)
                .extract()
                .response().getBody().asString();
    }

    @Step("Генерация списка тестовых данных пользователей размером {size}")
    public List<User> generateUsers(int size) {
        return Instancio.ofList(User.class).size(size)
                .ignore(field(User::getId))
                .set(field(User::getDeleted), false)
                .create();
    }

    @Step("Успешное создание пользователя")
    public static User sendUserCreateSuccessful(User user) {
        String result = RestAssured.given()
                .spec(new UserserviceSpecifications().requestSpec)
                .body(user)
                .log().all()
                .when()
                .post(UserserviceEndpoints.USERS)
                .then()
                .spec(new UserserviceSpecifications().responseSpec)
                .log().body()
                .extract()
                .response().getBody().asString();
        return JsonHelper.fromJson(User.class, result);
    }

    @Step("Получение пользователя по id {id}")
    public static User getUserById(String id) {
        String result = RestAssured.given()
                .spec(new UserserviceSpecifications().requestSpec)
                .pathParam("id", id)
                .when()
                .get(UserserviceEndpoints.USERS + "/{id}")
                .then()
                .spec(new UserserviceSpecifications().responseSpec)
                .extract().response().getBody().asString();
        return JsonHelper.fromJson(User.class, result);
    }

    public static User getUserByIdNotFound(String id) {
        String result = RestAssured.given()
                .spec(new UserserviceSpecifications().requestSpec)
                .pathParam("id", id)
                .when()
                .get(UserserviceEndpoints.USERS + "/{id}")
                .then()
                .spec(new UserserviceSpecifications().responseSpec404)
                .extract().response().getBody().asString();
        return JsonHelper.fromJson(User.class, result);

    }

    public static String deleteUser(String id) {
        return RestAssured.given()
                .spec(new UserserviceSpecifications().requestSpec)
                .pathParam("id", id)
                .when()
                .delete(UserserviceEndpoints.USERS + "/{id}")
                .then()
                .spec(new UserserviceSpecifications().responseSpec)
                .extract().response().getBody().asString();
    }

}
