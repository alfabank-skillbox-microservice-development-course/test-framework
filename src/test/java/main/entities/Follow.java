package main.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.Date;
@Data
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
public class Follow {

    private FollowId id;

    private User followerUser;

    private User followingUser;


    private Date createdAt;

    public Follow(User followerUser, User followingUser) {
        this.id = new FollowId(followerUser.getId(), followingUser.getId());
        this.followerUser = followerUser;
        this.followingUser = followingUser;
    }
}
