package main.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.Date;
import java.util.UUID;

@Data
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
public class User {

    private UUID id;

    private String firstname;

    private String surname;

    private String secondname;

    private Date birth;

    private String gender;

    private String email;

    private String phone;

    private Boolean deleted = Boolean.FALSE;

    private PhotoDTO photoDTO;

}
