package main.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.util.UUID;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor

public class FollowId {

    private UUID followerUserId;

    private UUID followingUserId;

}