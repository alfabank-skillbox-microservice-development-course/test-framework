package main.specifications;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class UserserviceSpecifications {

    public RequestSpecification requestSpec = new RequestSpecBuilder()
            .setBaseUri("http://localhost")
            .setPort(8181)
            .setAccept(ContentType.JSON)
            .setContentType(ContentType.JSON)
        .log(LogDetail.ALL)
    .build();


   public ResponseSpecification responseSpec = new ResponseSpecBuilder()
           .log(LogDetail.ALL)
           .expectStatusCode(200)
           .build();

    public ResponseSpecification responseSpec404 = new ResponseSpecBuilder()
            .log(LogDetail.ALL)
            .expectStatusCode(404)
            .build();
// можно задать одну спецификацию для всех запросов:
 //   RestAssured.requestSpecification = requestSpec;

    // или для отдельного:
  //  given().spec(requestSpec)...when().get(someEndpoint);

    // можно задать одну спецификацию для всех ответов:
    //RestAssured.responseSpecification = responseSpec;

    // или для отдельного:
    //given()...when().get(someEndpoint).then().spec(responseSpec)...;
}
