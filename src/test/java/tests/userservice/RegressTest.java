package tests.userservice;

import io.qameta.allure.Feature;
import main.entities.Follow;
import main.entities.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static main.steps.UserserviceSteps.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RegressTest extends BaseTest {



    @Test
    @Tag("REGRESS_01")
    @Tag("POSITIVE")
    @Tag("USERSERVICE_01")
    @Feature("Проверка микросервиса по управлению пользователями в рамках приложения skillgramm")
    @DisplayName("Создание/поиск пользователей -> создание/поиск подписок -> удаление подписок/пользователей - УСПЕШНО")
    void UsersCreatedTest() throws IOException, ClassNotFoundException {
        // Создание двоих пользователей
        User firstUserCreated = sendUserCreateSuccessful(usersInit.get(0));
        User secondUserCreated = sendUserCreateSuccessful(usersInit.get(1));

        //    Успешно получить их по ID.
        User firstUserGetResult = getUserById(firstUserCreated.getId().toString());
        User secondUserGetResult = getUserById(secondUserCreated.getId().toString());

        assertEquals(usersInit.get(0).getFirstname(), firstUserGetResult.getFirstname());
        assertEquals(usersInit.get(1).getFirstname(), secondUserGetResult.getFirstname());

        //    Подписать одного пользователя на другого.
        createFollow(firstUserCreated.getId(), secondUserCreated.getId());
        createFollow(secondUserCreated.getId(), firstUserCreated.getId());

        //    Проверить изменившиеся данные.
        List<Follow> firstUserFollows = findFollows(firstUserCreated.getId());
        List<Follow> secondUserFollows = findFollows(secondUserCreated.getId());

        assertTrue(firstUserFollows.stream().anyMatch(x -> x.getFollowingUser().getId().equals(secondUserCreated.getId())));
        assertTrue(secondUserFollows.stream().anyMatch(x -> x.getFollowingUser().getId().equals(firstUserCreated.getId())));

        //    Удалить подписку.
        String firstUserFollowSecondDeleteResult = deleteFollow(firstUserCreated.getId(), secondUserCreated.getId());
        assertEquals(String.format("Подписка пользователя с id %s на пользователя с id %s удалена",
         firstUserCreated.getId(), secondUserCreated.getId()), firstUserFollowSecondDeleteResult);

        String secondUserFollowFirstDeleteResult = deleteFollow(secondUserCreated.getId(), firstUserCreated.getId());
        assertEquals(String.format("Подписка пользователя с id %s на пользователя с id %s удалена",
          secondUserCreated.getId(), firstUserCreated.getId()), secondUserFollowFirstDeleteResult);

        //    Проверить изменившиеся данные.
        firstUserFollows = findFollows(firstUserCreated.getId());
        secondUserFollows = findFollows(secondUserCreated.getId());

        assertTrue(firstUserFollows.stream().noneMatch(x -> x.getFollowingUser().getId().equals(secondUserCreated.getId())));
        assertTrue(secondUserFollows.stream().noneMatch(x -> x.getFollowingUser().getId().equals(firstUserCreated.getId())));

        //    Удалить пользователей.
        String deleteFirstUserResult = deleteUser(firstUserCreated.getId().toString());
        assertEquals(String.format("Пользователь с uuid %s успешно удален",firstUserCreated.getId().toString()), deleteFirstUserResult);

        String deleteSecondUserResult = deleteUser(secondUserCreated.getId().toString());
        assertEquals(String.format("Пользователь с uuid %s успешно удален",secondUserCreated.getId().toString()), deleteSecondUserResult);

        //    Получить ошибку 404 по ID пользователей.
        getUserByIdNotFound(firstUserCreated.getId().toString());
        getUserByIdNotFound(secondUserCreated.getId().toString());
    }


}
