package tests.userservice;

import main.entities.User;
import main.steps.UserserviceSteps;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;


import java.util.List;


 class BaseTest {

    List<User> usersInit;
    String url = "8181";

    /**
     * Создание и инициализация тестовых данных
     */
    @BeforeEach
    void beforeEach() {
        usersInit = new UserserviceSteps().generateUsers(RandomStringUtils.randomAlphanumeric(2).length());
        url = "http://localhost:8181";
    }

}
